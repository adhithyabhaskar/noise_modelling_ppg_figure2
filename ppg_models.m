function [] = ppg_models(base_dir, param_dir, output_dir)
% clear, clc, close all;
% addpath(genpath(pwd));

%% from input folder for compiled version
[hcp_dir, ~,~] = readvars([base_dir,'input/vars.csv'], 'Delimiter','|','Range','2:2');
hcp_dir = hcp_dir{1};

task_list = readmatrix([base_dir,'input/tasks.txt'], 'OutputType', 'string','Delimiter','\n');
subject_list = readmatrix([base_dir,'input/subjects.txt'], 'OutputType', 'string');

%% paths
save_path = [output_dir,'output/']; % location to save output workspace variables
fig_path = [output_dir,'output/plots/']; % location to save figures
% base_dir = 'data/'; % location of subject_list.mat 
log_path = [base_dir,'logs/'];
log_file_path = [log_path, 'Estimate_PRF_log_', datestr(clock,'YYYYmmdd_HHMM'), '.txt'];

if isfolder(log_path)==0
    mkdir(log_path); end

if isfolder(output_dir)==0
    mkdir(output_dir); end
    
if isfolder(save_path)==0
    mkdir(save_path); end

if isfolder(fig_path)==0
    mkdir(fig_path); end

fileID = fopen(log_file_path,'w');

nSubj = length(subject_list);
nTask = length(task_list);
nScans = nSubj*nTask;

% read optimal parameters from figure7 script output
optimal_param_list = func_get_optimal_params(fileID, param_dir);

%% create phys data from Physio log files for all scans

tic;
for i = 1:length(subject_list)
    subject = subject_list(i);
    found_row = find(optimal_param_list(:,1)==subject);
    optimal1 = mean(str2double(optimal_param_list(found_row, 3)));
    optimal1 = round(optimal1/0.5)*0.5; % round to nearest 0.5
    optimal2 = round(mean(str2double(optimal_param_list(found_row, 4)))); % round to nearest integer
    func_print_and_file(fileID,"Processing subject %s, parameters: %d, %d\n", subject, optimal1, optimal2);
    func_create_phys_kscript(subject, hcp_dir, optimal1, optimal2, 0.5, fileID);
end
func_print_and_file(fileID,"Created phys.mat for %d subjects in %3.1f (minutes)\n", length(subject_list), toc/60);

 %% initialize constants

kFold=3;
Fs = 400; Ts = 1/Fs;  HPF_f = 0.008;
[filt_b,filt_a] = butter(2,HPF_f*2*Ts,'high');
temp_shift_scale = -3:0.1:3; nShifts = length(temp_shift_scale);

%% run loop across scans

T_del = 5;   % delete first 5 seconds from output
nDel = T_del*Fs;
M_order = 8;
% HRmean_all = zeros(nScans,1);
HRstd_all = zeros(nScans,1);
r_CPM_all = zeros(nScans,nShifts,M_order);
r_CPM_Amp_all = zeros(nScans,nShifts,M_order);
r_RETR_all = zeros(nScans,nShifts,M_order);

parfor c = 1 : nScans
    
    s = ceil(c/nTask);        run = c - (s-1)*nTask;
    subject = subject_list{s};         task = task_list{run};
    % func_print_and_file(fileID, 'Subject: %s     (%d/%d);   Run: %d/%d    \n',subject,s,nSubj,run,length(task_list))
    fprintf( 'Subject: %s     (%d/%d);   Run: %d/%d    \n',subject,s,nSubj,run,length(task_list))
    [Ts,Fs,~,~,PPGlocs,HR,~, cardiac,~,~,~,~] =  func_load_scan_kscript(subject,task,hcp_dir);
    
    HRstd = std(HR);
    HRstd_all(c) = HRstd;
    HRmean = mean(HR);
    
    voxel = cardiac;
    voxel(1:nDel) = []; voxel(end-nDel:end) = [];
    NV = length(voxel);
    voxel = filtfilt(filt_b,filt_a,voxel);
    N = length(cardiac); time = 0:Ts:(N-1)*Ts;
    
    block_length=round((NV)/kFold); ind_blocks=zeros(kFold,2);
    for i=1:kFold
        ind_blocks(i,1)=1+(i-1)*block_length;
        ind_blocks(i,2)=min(i*block_length,NV);  ind_blocks(kFold,2) = NV;
    end
    
    memory = 60/HRmean;
    CPM_IR = func_CPM_cos(Ts, memory, M_order);
    u = zeros(size(time)); uA = u;
    nPeaks = length(PPGlocs);
    for i = 1:nPeaks
        t = PPGlocs(i);
        [~,loc] = min(abs(time-t));
        u(loc) = 1;
        uA(loc) = cardiac(loc);
    end
    
    CPM_regr_all = zeros(N,M_order);
    CPM_Amp_regr_all = zeros(N,M_order);
    for m = 1:M_order*2
        u_conv = conv(u,CPM_IR(:,m)); u_conv = u_conv(1:N);
        CPM_regr_all(:,m) = u_conv(:);
        x = conv(uA,CPM_IR(:,m)); x = x(1:N);
        CPM_Amp_regr_all(:,m) = x(:);
    end
    CPM_regr_all = filtfilt(filt_b,filt_a,CPM_regr_all);
    CPM_Amp_regr_all = filtfilt(filt_b,filt_a,CPM_Amp_regr_all);
    RETR_regr_all = RETR_Card_regressors_v2(time,PPGlocs,M_order);
    RETR_regr_all = filtfilt(filt_b,filt_a,RETR_regr_all);

    %%  ----------------------------------------------
    
    r_CPM_shift = zeros(nShifts,M_order);
    r_CPM_Amp_shift = zeros(nShifts,M_order);
    r_RETR_shift = zeros(nShifts,M_order);
    for  c_temp = 1:nShifts
        shift = temp_shift_scale(c_temp);
        
        CPM_regr = zeros(NV,M_order);
        CPM_Amp_regr = zeros(NV,M_order);
        RETR_regr = zeros(NV,M_order);
        for m = 1:M_order*2
            ind = 1:NV; ind = round(ind + nDel + shift*Fs);
            CPM_regr(:,m) = CPM_regr_all(ind,m);
            CPM_Amp_regr(:,m) = CPM_Amp_regr_all(ind,m);
            RETR_regr(:,m) = RETR_regr_all(ind,m);
        end
        
        regr_CPM = [CPM_regr, ones(NV,1)];
        regr_CPM_Amp = [CPM_Amp_regr, ones(NV,1)];
        RETR_regr = [RETR_regr, ones(NV,1)];
        
        %% CPM  ---------------------------------------------
        
        regr = regr_CPM;
        r_valid_k=zeros(M_order,kFold);
        for k=1:kFold
            ind_valid = ind_blocks(k,1):ind_blocks(k,2);
            ind_train = 1:NV; ind_train(ind_valid)=[];
            if kFold==1, ind_train=1:NV; end
            
            regr_train = regr(ind_train,:);  % regr_train(:,1:end-1) = detrend(regr_train(:,1:end-1),'linear');
            regr_valid = regr(ind_valid,:); % regr_valid(:,1:end-1) = detrend(regr_valid(:,1:end-1),'linear');
            voxel_train =voxel(ind_train);    voxel_valid = voxel(ind_valid);
            %             voxel_train = detrend(voxel(ind_train),'linear');    voxel_valid = detrend(voxel(ind_valid),'linear');
            for m = 1:M_order
                ind_regr = [1:2*m,size(regr,2)];
                B = regr_train(:,ind_regr)\voxel_train;     yPred = regr_valid(:,ind_regr)*B;
                r_valid_k(m,k) = corr(voxel_valid,yPred) ;
            end
        end
        r_CPM_shift(c_temp,:) = mean(r_valid_k,2);
        
        %% CPM_Amp ---------------------------------------------
        
        regr = regr_CPM_Amp;
        r_valid_k=zeros(M_order,kFold);
        for k=1:kFold
            ind_valid = ind_blocks(k,1):ind_blocks(k,2);
            ind_train = 1:NV; ind_train(ind_valid)=[];
            if kFold==1, ind_train=1:NV; end
            
            regr_train = regr(ind_train,:); % regr_train(:,1:end-1) = detrend(regr_train(:,1:end-1),'linear');
            regr_valid = regr(ind_valid,:); % regr_valid(:,1:end-1) = detrend(regr_valid(:,1:end-1),'linear');
            voxel_train =voxel(ind_train);    voxel_valid = voxel(ind_valid);
            %             voxel_train = detrend(voxel(ind_train),'linear');    voxel_valid = detrend(voxel(ind_valid),'linear');
            for m = 1:M_order
                ind_regr = [1:2*m,size(regr,2)];
                B = regr_train(:,ind_regr)\voxel_train;     yPred = regr_valid(:,ind_regr)*B;
                r_valid_k(m,k) = corr(voxel_valid,yPred);
            end
            
        end
        r_CPM_Amp_shift(c_temp,:) = mean(r_valid_k,2);
        
        %% RETROICOR  ---------------------------------------------
        
        regr = RETR_regr;
        r_valid_k=zeros(M_order,kFold);
        for k=1:kFold
            ind_valid = ind_blocks(k,1):ind_blocks(k,2);
            ind_train = 1:NV; ind_train(ind_valid)=[];
            if kFold==1, ind_train=1:NV; ind_valid = ind_train; end
            
            regr_train = regr(ind_train,:);  %  regr_train(:,1:end-1) = detrend(regr_train(:,1:end-1),'linear');
            regr_valid = regr(ind_valid,:); % regr_valid(:,1:end-1) = detrend(regr_valid(:,1:end-1),'linear');
            voxel_train =voxel(ind_train);    voxel_valid = voxel(ind_valid);
            %             voxel_train = detrend(voxel(ind_train),'linear');    voxel_valid = detrend(voxel(ind_valid),'linear');
            
            for m = 1:M_order
                ind_regr = [1:2*m,size(regr,2)];
                B = regr_train(:,ind_regr)\voxel_train;     yPred = regr_valid(:,ind_regr)*B;
                r_valid_k(m,k) = corr(voxel_valid,yPred);
            end
            
        end
        r_RETR_shift(c_temp,:) = mean(r_valid_k,2);

    end
    r_CPM_all(c,:,:) = r_CPM_shift;
    r_CPM_Amp_all(c,:,:) = r_CPM_Amp_shift;
    r_RETR_all(c,:,:) = r_RETR_shift;

end
save([save_path, 'workspace.mat']);
func_print_and_file(fileID,'Time elapsed is %3.1f (minutes)\n', toc/60);
func_create_figure(fig_path, r_RETR_all,temp_shift_scale,nSubj,r_CPM_all,r_CPM_Amp_all,HRstd_all)
% save([save_path, 'workspace_', datestr(clock,'YYYYmmdd_HHMMSS'), '.mat']);
end