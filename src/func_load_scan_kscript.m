function [Ts,Fs,TR, trig,PPGlocs, HR, resp, cardiac,movRegr,resp_10,RVT,RF] = func_load_scan_kscript(subject, task, baseDir)
%FUNC_LOAD_SCAN_KSCRIPT Summary of this function goes here
%   Detailed explanation goes here

filepath_movRegr=[baseDir, subject,'/MNINonLinear/Results/Movement_Regressors_dt.txt'];
movRegr=load(filepath_movRegr);  movRegr=[movRegr, movRegr.^2];

filepath_MRacq=[baseDir, subject,'/MNINonLinear/physio/', task, '_phys_kscript.mat'];
load(filepath_MRacq,"Fs","trig","TR","cardiac_filt", "resp","PPGlocs","HR","resp_10","RVT","RF"); Ts = 1/Fs;
cardiac=cardiac_filt;

end