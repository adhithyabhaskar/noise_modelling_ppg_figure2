function [] = func_create_figure(fig_path, r_RETR_all,temp_shift_scale,nSubj,r_CPM_all,r_CPM_Amp_all,HRstd_all)
    %func_create_figure Summary of this function goes here
    %   Detailed explanation goes here
if isfolder(fig_path)==0
    mkdir(fig_path); end

x = squeeze(r_RETR_all(:,:,8));
NP = length(temp_shift_scale);
x_sbj = zeros(nSubj,NP);
for i = 1:nSubj
    ind = (i-1)*4 + (1:4);
    tmp = x(ind,:);
    x_sbj(i,:) = mean(tmp,1);
end
RETR_sbj = x_sbj;
r_mean_RETR = mean(x_sbj);
r_std_RETR = std(x_sbj)/sqrt(nSubj);

x = squeeze(r_CPM_all(:,:,8));
NP = length(temp_shift_scale);
x_sbj = zeros(nSubj,NP);
for i = 1:nSubj
    ind = (i-1)*4 + (1:4);
    tmp = x(ind,:);
    x_sbj(i,:) = mean(tmp,1);
end
CPM_CA_sbj = x_sbj;
r_mean_CPM_CA = mean(x_sbj);
r_std_CPM_CA = std(x_sbj)/sqrt(nSubj);

x = squeeze(r_CPM_Amp_all(:,:,8));
NP = length(temp_shift_scale);
x_sbj = zeros(nSubj,NP);
for i = 1:nSubj
    ind = (i-1)*4 + (1:4);
    tmp = x(ind,:);
    x_sbj(i,:) = mean(tmp,1);
end
CPM_VA_sbj = x_sbj;
r_mean_CPM_VA = mean(x_sbj);
r_std_CPM_VA = std(x_sbj)/sqrt(nSubj);

figure('visible','off')
shadedErrorBar(-temp_shift_scale,r_mean_RETR,r_std_RETR,'lineprops','-r') , hold on
shadedErrorBar(-temp_shift_scale,r_mean_CPM_CA,r_std_CPM_CA,'lineprops','-k') , hold on
shadedErrorBar(-temp_shift_scale,r_mean_CPM_VA,r_std_CPM_VA,'lineprops','-g') , hold on
grid on
xlim([-2 2])
ylim([0.5 1])
xlabel('Lag time (s)'), ylabel('Mean correlation')
saveas(gcf, [fig_path, 'Figure2_e.png'])

figure('visible','off')
plot(-temp_shift_scale,r_mean_RETR,'-r') , hold on
plot(-temp_shift_scale,r_mean_CPM_CA,'-k') , hold on
plot(-temp_shift_scale,r_mean_CPM_VA,'-g') , hold on
grid on
xlim([-2 2])
% yticks(0.5:0.1:1)
ylim([0.5 1])
xlabel('Lag time (s)'), ylabel('Mean correlation')
saveas(gcf, [fig_path, 'Figure_2e_no_errorbars.png'])

%%  -----------------
% Figure 2 (f)

x1 = RETR_sbj(:,32);
x2 = CPM_CA_sbj(:,33);
% x3 = CPM_VA_sbj(:,36);

% mean(x1)
% mean(x2)
% mean(x3)

% [ttest_h, ttest_p] = ttest(x1,x2);

dx_sbj = x2-x1;
dx_relat = (x2-x1)*100./x1;

HRstd_sbj = reshape(HRstd_all,[4 nSubj])';
HRstd_sbj = mean(HRstd_sbj,2);

x = HRstd_sbj;
% y = dx_sbj;
y = dx_relat;
r = corr(x,y);
model = fitlm(x,y);
pVal = model.Coefficients.pValue(2);
Par = model.Coefficients.Estimate; P12 = Par(1)+Par(2)*1;
x_scal = (min(x)-0.2:0.0001:max(x)+0.2);
y_scal = Par(1) + Par(2)*x_scal;

figure('visible','off')
scatter(x,y,50,'k','LineWidth',2)
ax=gca; ax.FontWeight='bold'; hold on
plot(x_scal,y_scal,'r','LineWidth',3)
xlabel('Heart rate variability (bpm)')
ylabel('?r(RETR-->CPM_{CA})')
ylabel('% improvement')
grid on
xlim([1 10.5])
ylim([-2 3])
saveas(gcf, [fig_path, 'Figure_2f.png'])

end