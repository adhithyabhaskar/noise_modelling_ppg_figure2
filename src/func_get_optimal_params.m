function [optimal_parameter_list] = func_get_optimal_params(fileID, param_path)
%FUNC_GET_OPTIMAL_PARAMS Summary of this function goes here
%   Detailed explanation goes here

% param_path =  strjoin(["/persistent-data/HCP/output_",nSubj,"subjv2/"],"");
files = dir(join([param_path, '**/optimal_params*.mat'],""));
num_mat_found = numel(files);
func_print_and_file(fileID, "Number of processed scans found: %d\n", num_mat_found)

optimal_parameter_list = strings([num_mat_found, 4]);
for i = 1:num_mat_found
    file_path = fullfile(files(i).folder, files(i).name);
    load(file_path, 'optimal_params');
    optimal_parameter_list(i,:) = optimal_params;
end

end