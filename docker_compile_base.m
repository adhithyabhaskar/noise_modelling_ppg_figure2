diary on
diary('logs/log_docker_compile.txt')
appFile = compiler.build.StandaloneApplicationOptions('ppg_models.m', ...
    'OutputDir','output/compiled', ...
    'TreatInputsAsNumeric', false)
buildResults = compiler.build.standaloneApplication(appFile)
compiler.package.docker(buildResults, ...
    'ImageName','ppg_models', ...
    'DockerContext', 'output/docker')
diary off